resource "google_compute_network" "network" {
  name                    = "${var.cluster_name}-network${var.cluster_name_suffix}"
  project                 = var.project_id
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "subnetwork" {
  name                     = "${var.cluster_name}-subnetwork${var.cluster_name_suffix}"
  project                  = var.project_id
  ip_cidr_range            = var.subnet_ip_cidr_block
  region                   = var.region
  network                  = google_compute_network.network.self_link
  private_ip_google_access = true
  secondary_ip_range {
    range_name    = "${var.cluster_name}-pods-secondary-ip-range${var.cluster_name_suffix}"
    ip_cidr_range = var.cluster_ipv4_cidr_block
  }
  secondary_ip_range {
    range_name    = "${var.cluster_name}-services-secondary-ip-range${var.cluster_name_suffix}"
    ip_cidr_range = var.services_ipv4_cidr_block
  }
}

resource "google_compute_address" "k8s_ingress_ip" {
  name         = "${var.cluster_name}-ingress-ip${var.cluster_name_suffix}"
  address_type = "EXTERNAL"
  region       = var.region
}

resource "google_compute_router" "router" {
  name    = "${var.cluster_name}-router${var.cluster_name_suffix}"
  project = var.project_id
  region  = var.region
  network = google_compute_network.network.self_link
}

resource "google_compute_router_nat" "advanced-nat" {
  name                   = "${var.cluster_name}-nat${var.cluster_name_suffix}"
  project                = var.project_id
  router                 = google_compute_router.router.name
  region                 = var.region
  nat_ip_allocate_option = "AUTO_ONLY"
  # TODO Manual NAT outbound addresses (to avoid external IP saturation)
  # nat_ip_allocate_option             = "MANUAL_ONLY"
  # nat_ips                          = ["${google_compute_address.address.*.self_link}"]
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  min_ports_per_vm                   = "8192"
  tcp_transitory_idle_timeout_sec    = "30"
  tcp_established_idle_timeout_sec   = "1200"
  udp_idle_timeout_sec               = "30"
  icmp_idle_timeout_sec              = "30"
  subnetwork {
    name                    = google_compute_subnetwork.subnetwork.self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
  log_config {
    filter = "ERRORS_ONLY"
    enable = true
  }
}

# Workaround to fix to allow the autoinject error
# Updated to istio 1.5 (15017)
resource "google_compute_firewall" "istio_rule" {
  name          = "istio-allow-firewall"
  project       = var.project_id
  network       = google_compute_network.network.self_link
  priority      = 1000
  direction     = "INGRESS"
  source_ranges = [var.master_ip_cidr_block]
  // As we don't know the network tag, we allow for all
  //target_tags   = ["gke-gke-on-vpc-cluster-349a7ec2-node"]

  allow {
    protocol = "tcp"
    ports    = ["9443", "15017"]
  }

  depends_on = [google_compute_network.network]
}
