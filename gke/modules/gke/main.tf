module "gke" {
  source            = "terraform-google-modules/kubernetes-engine/google//modules/beta-private-cluster-update-variant"
  version           = "7.3.0"
  project_id        = var.project_id
  name              = "${var.cluster_name}${var.cluster_name_suffix}"
  regional          = var.regional
  region            = var.region
  network           = google_compute_network.network.name
  subnetwork        = google_compute_subnetwork.subnetwork.name
  ip_range_pods     = google_compute_subnetwork.subnetwork.secondary_ip_range[0].range_name
  ip_range_services = google_compute_subnetwork.subnetwork.secondary_ip_range[1].range_name
  //create_service_account  = true
  enable_private_endpoint = false
  enable_private_nodes    = true
  master_ipv4_cidr_block  = var.master_ip_cidr_block
  kubernetes_version      = var.kubernetes_version
  release_channel         = var.release_channel

  cluster_resource_labels = {
    project     = var.project_id
    environment = var.environment
  }

  master_authorized_networks = [
    {
      # External networks that can access the Kubernetes cluster master through HTTPS
      cidr_block   = "0.0.0.0/0"
      display_name = "All"
    }
  ]
  istio    = false
  cloudrun = false

  remove_default_node_pool = true
  node_pools = [
    {
      name               = "updatable-node-pool",
      machine_type       = var.node_pool_machine_type,
      initial_node_count = var.node_pool_initial_count,
      min_count          = var.node_pool_min_count,
      max_count          = var.node_pool_max_count,
      preemptible        = true
    },
  ]

  skip_provisioners = true # disable local-exec provisiones (e.g. wait_for_cluster)
}

data "google_client_config" "default" {
}

// Allow nodes SA to download images from the container registry
resource "google_project_iam_member" "gke_service_account_iam" {
  project = var.project_id
  role    = "roles/storage.objectViewer"
  member  = "serviceAccount:${module.gke.service_account}"
}
