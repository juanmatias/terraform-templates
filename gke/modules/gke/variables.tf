variable "project_id" {
  description = "The project ID to host the cluster in"
}

variable "cluster_name" {
  description = "The name for the GKE cluster"
}

variable "regional" {
  type        = bool
  description = "Whether is a regional cluster (zonal cluster if set false. WARNING: changing this after cluster creation is destructive!)"
  default     = true
}

variable "region" {
  description = "The region to host the cluster in"
}

variable "zones" {
  type        = list(string)
  description = "The zone to host the cluster in (required if is a zonal cluster)"
  default     = []
}

variable "cluster_name_suffix" {
  description = "A suffix to append to the default cluster name"
  default     = ""
}

variable "subnet_ip_cidr_block" {
  type        = string
  description = "IPv4 CIDR Block for subnetwork"
  default     = "10.0.0.0/17"
}

variable "master_ip_cidr_block" {
  type        = string
  description = "IPv4 CIDR Block for the Master nodes"
  default     = "172.16.0.0/28"
}

variable "cluster_ipv4_cidr_block" {
  type        = string
  description = "IPv4 CIDR Block for Kubernetes Pods"
  default     = "192.168.0.0/18"
}

variable "services_ipv4_cidr_block" {
  type        = string
  description = "IPv4 CIDR Block for Kubernetes services"
  default     = "192.168.64.0/18"
}

variable "node_pool_min_count" {
  description = "Node Pool minimum number of nodes (per zone)"
  default     = 1
}

variable "node_pool_max_count" {
  description = "Node Pool maximum number of nodes (per zone)"
  default     = 3
}

variable "node_pool_initial_count" {
  description = "Node Pool initial number of nodes (per zone)"
  default     = 1
}

variable "node_pool_machine_type" {
  type        = string
  description = "Compute Engine machine type for the cluster nodes"
  default     = "n1-standard-2"
}

variable "kubernetes_version" {
  type        = string
  description = "Kubernetes version for control plane. If not node_version specified, we'll be used for nodes as well"
}

variable "release_channel" {
  type        = string
  description = "(Beta) The release channel of this cluster. Accepted values are `UNSPECIFIED`, `RAPID`, `REGULAR` and `STABLE`. Defaults to `UNSPECIFIED`."
  default     = null
}

variable "environment" {
  description = "Environment identifier"
}
