Login:

gcloud auth login

Get your credentials and save them to a file called `env/credentials.json`.

cd env

terraform apply

Connect

gcloud container clusters get-credentials kungfoo-test --region us-central1 --project speedy-league-274700

