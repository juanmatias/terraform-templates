module "gke" {
  source               = "../modules/gke"
  project_id           = var.project_id
  region               = var.region
  cluster_name         = var.cluster_name
  subnet_ip_cidr_block = "10.160.0.0/17"
  kubernetes_version   = var.kubernetes_version
  environment          = var.environment
}
