variable "project_id" {
  description = "The project ID to host the cluster in"
}

variable "region" {
  description = "The region to host the cluster in"
}

variable "environment" {
  description = "Environment identifier"
}

variable "cluster_name" {
  description = "The name for the GKE cluster"
}

variable "kubernetes_version" {
  type        = string
  description = "Kubernetes version for control plane. If not node_version especified, we'll be used for nodes as well"
  default     = "1.15.11-gke.9"
}
