
# ############################################
# Providers versions
# ############################################

terraform {
  required_version = "~> 0.11.13"
}

provider "aws" {
  version = "~> 2.43"

  region = "us-east-2"
}

module "database" {
  source = "terraform-aws-modules/rds/aws"
  version = "~> 1.28.0"

  identifier = "kungfu"

  engine            = "postgres"
  engine_version    = "11.4"
  instance_class    = "db.t3.small"
  allocated_storage = 100

  auto_minor_version_upgrade = false
  allow_major_version_upgrade = true
  apply_immediately = true

  name = "kungfudb"
  username = "kungfu"
  password = "dontFocusOnTheFingerPointingToTheMoon"
  port     = "5432"

  multi_az = true

  storage_encrypted = false

  vpc_security_group_ids = ["sg-0ec97ea6ad9e816a4"]

  maintenance_window = "Mon:01:30-Mon:03:30"
  backup_window      = "00:30-01:30"

  # disable backups to create DB faster
  backup_retention_period = 7

  tags = {
    Terraform = "true"
    Environment = "test"
    Project     = "kungfu"
    Component   = "database"
  }

  # DB subnet group
  subnet_ids = ["subnet-0a905fbd2b0f583cf","subnet-0a59090d5d1dd81ef"]

  # DB parameter group
  create_db_parameter_group = true
  family = "postgres11"
  parameters = [
    {
      name = "log_statement"
      value = "all"
    }
  ]

  create_monitoring_role = false
  monitoring_role_arn = "arn:aws:iam::111111111111:role/rds-monitoring-role"
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]
  monitoring_interval = "30"

  # DB option group
  major_engine_version = "11.4"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "kungfu-snapshot"

  # Database Deletion Protection
  # Later, if you need to destroy the database, first will need to set this to false
  deletion_protection = true
}

