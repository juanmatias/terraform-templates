#!/usr/bin/env python3
import base64

def lambda_handler(event, context):

   print('#SUBSCRIBER')
   message = 'The event is {} !'.format(event)
   print('Msg:')
   print(message)

   if 'rmqMessagesByQueue' in event and 'thequeuetohell::/' in event['rmqMessagesByQueue']:
      for msg in event['rmqMessagesByQueue']['thequeuetohell::/']:
         if 'data' in msg:
            decodedBytes = base64.b64decode(msg['data'])
            decodedStr = str(decodedBytes, "utf-8")
            print(f"Receibed message is: {decodedStr}")
         else:
            print('no data in msg')
   else:
      print('wrong event format?')
   return {

       'message' : message

   }
