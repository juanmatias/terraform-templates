# Lambda-RabbitMQ trigger

This example creates a RabbitMQ broker and two lambdas. 

One lambda publishes messages into the queue.

The other one is triggered on message arrival.

It is almost self explanatory, so go and dig into the files.
