#!/usr/bin/env python3

import ssl
import pika
import random
import os

class BasicPikaClient:

    def __init__(self, rabbitmq_broker_id, rabbitmq_user, rabbitmq_password, region):

        # SSL Context for TLS configuration of Amazon MQ for RabbitMQ
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        ssl_context.set_ciphers('ECDHE+AESGCM:!ECDSA')

        url = f"amqps://{rabbitmq_user}:{rabbitmq_password}@{rabbitmq_broker_id}.mq.{region}.amazonaws.com:5671"
        parameters = pika.URLParameters(url)
        parameters.ssl_options = pika.SSLOptions(context=ssl_context)

        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()

class BasicMessageSender(BasicPikaClient):

    def declare_queue(self, queue_name):
        print(f"Trying to declare queue({queue_name})...")
        self.channel.queue_declare(queue=queue_name)

    def send_message(self, exchange, routing_key, body):
        channel = self.connection.channel()
        channel.basic_publish(exchange=exchange,
                              routing_key=routing_key,
                              body=body)
        print(f"Sent message. Exchange: {exchange}, Routing Key: {routing_key}, Body: {body}")

    def close(self):
        self.channel.close()
        self.connection.close()

def lambda_handler(event, context):

    print('#PUBLISHER')
    print('Getting values from env:')

    RABBITMQ_QUEUE    = os.getenv("RABBITMQ_QUEUE")
    RABBITMQ_ID       = os.getenv("RABBITMQ_ID")
    RABBITMQ_USER       = os.getenv("RABBITMQ_USER")
    RABBITMQ_PASSWORD   = os.getenv("RABBITMQ_PASSWORD")
    REGION   = os.getenv("REGION")

    print(RABBITMQ_ID)
    print(RABBITMQ_USER)
    print(RABBITMQ_PASSWORD)
    print(REGION)
    # Initialize Basic Message Sender which creates a connection
    # and channel for sending messages.
    basic_message_sender = BasicMessageSender(
        RABBITMQ_ID,
        RABBITMQ_USER,
        RABBITMQ_PASSWORD,
        REGION
    )


    # Declare a queue
    basic_message_sender.declare_queue(RABBITMQ_QUEUE)

    msg = f"Hello World, Pikachu! {random.randint(0,1000)}"
    print(f"Sending message: {msg}")
    # Send a message to the queue.
    basic_message_sender.send_message(exchange="", routing_key=RABBITMQ_QUEUE, body=bytes(msg,'utf-8'))

    # Close connections.
    basic_message_sender.close()
