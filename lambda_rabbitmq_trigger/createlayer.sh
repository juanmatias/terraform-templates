#!/usr/bin/env bash

cd publisher_layer/python
pip install --disable-pip-version-check -r requirements.txt -t .
