# ############################################
#
# To allow the usage of RabbitMQ as a source event,
# according to https://docs.aws.amazon.com/lambda/latest/dg/with-mq.html
# there is a need of having one of these:
# - Configure one NAT gateway per public subnet.
# - Create a connection between your Amazon VPC and Lambda.
# For the later see https://docs.aws.amazon.com/lambda/latest/dg/configuration-vpc-endpoints.html
#
# In the code look for REQUIREMENTS tag
#
# ############################################
# PROVIDERS
# ############################################
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
# here using an AWS profile of my own, change it
provider "aws" {
  region                   = "us-east-1"
  profile                  = "myown-terraform"
}

# ############################################
# VARIABLES
# ############################################
variable "queuename" {
  type    = string
  default = "thequeuetohell"
}
variable "region" {
  type    = string
  default = "us-east-1"
}
variable "vpccidr" {
  type    = string
  default = "10.0.0.0/16"
}
# the account id
data "aws_caller_identity" "current" {}

# ############################################
# VPC
# ############################################
# It is important to set DNS Support and Hostnames to true
# so the publisher lambda can reach the mq broker
module "red" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.12.0"

  name = "kungfoo-tests-vpc"
  cidr = var.vpccidr

  azs             = ["${ var.region }a", "${ var.region }b", "${ var.region }c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]

  # REQUERIMENTS
  # for the NAT G option set this line to true:
  #
  enable_nat_gateway = false
  #
  # For the endpoint option look for REQUIREMENTS below
  enable_vpn_gateway = true
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Terraform = "true"
    Environment = "test"
  }
}
# REQUIREMENTS
# for the endpoint options uncomment this code:
resource "aws_vpc_endpoint" "lambda_endpoint" {
  vpc_id            = module.red.vpc_id
  service_name      = "com.amazonaws.${ var.region }.lambda"
  vpc_endpoint_type = "Interface"
  subnet_ids = [module.red.private_subnets[0]]
  security_group_ids = [ aws_security_group.sg-endpoint.id ]
  private_dns_enabled = true
  depends_on = [aws_vpc_endpoint.sts_endpoint, aws_vpc_endpoint.secretsmanager_endpoint]
}
resource "aws_vpc_endpoint" "sts_endpoint" {
  vpc_id            = module.red.vpc_id
  service_name      = "com.amazonaws.${ var.region }.sts"
  vpc_endpoint_type = "Interface"
  subnet_ids = [module.red.private_subnets[0]]
  security_group_ids = [ aws_security_group.sg-endpoint.id ]
  private_dns_enabled = true
}
resource "aws_vpc_endpoint" "secretsmanager_endpoint" {
  vpc_id            = module.red.vpc_id
  service_name      = "com.amazonaws.${ var.region }.secretsmanager"
  vpc_endpoint_type = "Interface"
  subnet_ids = [module.red.private_subnets[0]]
  security_group_ids = [ aws_security_group.sg-endpoint.id ]
  private_dns_enabled = true
}
# ############################################
# SECRETS
# ############################################
# We set a secret with the basic auth for RabbitMQ
resource "random_string" "secretname" {
    length  = 10
    special = false
}
resource "random_string" "username" {
    length  = 10
    special = false
}
resource "random_password" "password" {
    length  = 20
    special = false
}
resource "aws_secretsmanager_secret" "user_default" {
name        = random_string.secretname.result
}

resource "aws_secretsmanager_secret_version" "secret_val" {
  secret_id     = aws_secretsmanager_secret.user_default.id
  secret_string = jsonencode({"username": "${random_string.username.result}", "password": "${random_password.password.result}"})
}

data "aws_secretsmanager_secret" "user_default" {
  arn = aws_secretsmanager_secret.user_default.arn
}

data "aws_secretsmanager_secret_version" "secret_val" {
  secret_id = data.aws_secretsmanager_secret.user_default.arn
}

# ############################################
# SECURITY GROUPS
# ############################################
resource "aws_security_group" "sg-broker" {
  name = "Broker SG"
  description = "Broker SG"
  vpc_id = module.red.vpc_id
  ingress {
    from_port   = 5671
    to_port     = 5671
    protocol    = "tcp"
    cidr_blocks = [var.vpccidr]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.vpccidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.vpccidr]
    #cidr_blocks      = ["0.0.0.0/0"]
  }

 tags = {
    Name = "Kungfoo Security Group Broker"
  }
}
resource "aws_security_group" "sg-subscriber" {
  name = "Subscriber SG"
  description = "Subscriber SG"
  vpc_id = module.red.vpc_id
  egress {
    from_port   = 5671
    to_port     = 5671
    protocol    = "tcp"
    cidr_blocks = [var.vpccidr]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.vpccidr]
  }

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.vpccidr]
  }

 tags = {
    Name = "Kungfoo Security Group Subscrier"
  }
}
resource "aws_security_group" "sg-endpoint" {
  name = "Endpoint SG"
  description = "Endpoint SG"
  vpc_id = module.red.vpc_id

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.vpccidr]
    #cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.vpccidr]
    #cidr_blocks      = ["0.0.0.0/0"]
  }

 tags = {
    Name = "Kungfoo Security Group Endpoint"
  }
}
# ############################################
# LAMBDAS
# ############################################
#
# Pika Layer
resource "null_resource" "health_check" {

 provisioner "local-exec" {

    command = "/bin/bash createlayer.sh"
  }
}

data "archive_file" "zip_the_python_code3" {
    type        = "zip"
    source_dir  = "${path.module}/publisher_layer/"
    output_path = "${path.module}/python/publisher_layer.zip"
}

resource "aws_lambda_layer_version" "lambda_layer" {
  filename   = "${path.module}/python/publisher_layer.zip"
  layer_name = "pikalayer"

  compatible_runtimes = ["python3.8"]
}

# the actual lambdas
#
data "archive_file" "zip_the_python_code" {
    type        = "zip"
    source_dir  = "${path.module}/subscriber/"
    output_path = "${path.module}/python/subscriber.zip"
}

data "archive_file" "zip_the_python_code2" {
    type        = "zip"
    source_dir  = "${path.module}/publisher/"
    output_path = "${path.module}/python/publisher.zip"
}

resource "aws_lambda_function" "terraform_lambda_func" {
    filename                       = "${path.module}/python/subscriber.zip"
    source_code_hash                    = "${path.module}/python/subscriber.zip"
    function_name                  = "Triggered_Test_Lambda_Function"
    role                           = aws_iam_role.lambda_role.arn
    handler                        = "index.lambda_handler"
    runtime                        = "python3.8"
    depends_on                     = [aws_iam_role_policy_attachment.lambda_sqs_role_policy, aws_iam_role_policy_attachment.lambda_sqs_subs_role_policy]
    vpc_config {
      subnet_ids         = [module.red.private_subnets[0]]
      security_group_ids = [aws_security_group.sg-subscriber.id]
    }
}

resource "aws_lambda_function" "terraform_lambda_func2" {
    filename                       = "${path.module}/python/publisher.zip"
    source_code_hash                    = "${path.module}/python/publisher.zip"
    function_name                  = "Pub_Test_Lambda_Function"
    role                           = aws_iam_role.lambda_role.arn
    handler                        = "index.lambda_handler"
    layers                         = [aws_lambda_layer_version.lambda_layer.arn]
    runtime                        = "python3.8"
    depends_on                     = [aws_iam_role_policy_attachment.lambda_sqs_role_policy, aws_iam_role_policy_attachment.lambda_sqs_subs_role_policy]
    vpc_config {
      subnet_ids         = [module.red.private_subnets[0]]
      security_group_ids = [module.red.default_security_group_id]
    }

    environment {

        variables = {

          RABBITMQ_QUEUE    = var.queuename
          RABBITMQ_ID       = aws_mq_broker.rabbitmq.id
          RABBITMQ_USER       = random_string.username.result
          RABBITMQ_PASSWORD   = random_password.password.result
          REGION = var.region


        }

    }
}

# ############################################
# THE BROKER
# ############################################
resource "aws_mq_broker" "rabbitmq" {

    broker_name = "elconejito"

    engine_type                = "RabbitMQ"
    engine_version             = "3.8.22"
    host_instance_type         = "mq.t3.micro"
    auto_minor_version_upgrade = false
    publicly_accessible        = false
    subnet_ids         = [module.red.private_subnets[0]]
    #security_groups = [module.red.default_security_group_id]
    security_groups = [aws_security_group.sg-broker.id]

    user {

        username = random_string.username.result
        password = random_password.password.result

    }

    depends_on = [aws_vpc_endpoint.lambda_endpoint]

}


# ############################################
# IAM
# ############################################
data "aws_iam_policy_document" "lambda_role_iam_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "lambda_role" {
  name               = "SQSSubscriberLambdaRole"
  assume_role_policy = data.aws_iam_policy_document.lambda_role_iam_policy.json
}

resource "aws_iam_role_policy_attachment" "lambda_sqs_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole"
}
resource "aws_iam_role_policy_attachment" "lambda_sqs_subs_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonMQReadOnlyAccess"
}
resource "aws_iam_policy" "policy-secret" {
  name        = "getfromsecret-mqauth-policy"
  description = "Get basic auth from secrets"
  policy = jsonencode(
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": [
                    "secretsmanager:GetSecretValue",
                ],
                "Effect": "Allow",
                "Resource": "${ data.aws_secretsmanager_secret.user_default.arn }"
            }
        ]
    }
    )
}
resource "aws_iam_role_policy_attachment" "lambda_sqs_secret_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.policy-secret.arn
}

resource "aws_iam_policy" "policy-ec2" {
  name        = "ec2-policy"
  description = "Create ifaces in EC2"
  policy = jsonencode(
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": [
                    "ec2:CreateNetworkInterface", "ec2:DescribeNetworkInterfaces", "ec2:DeleteNetworkInterface",
                ],
                "Effect": "Allow",
                "Resource": "*"
                #"Resource": "*",
                #"Condition": {
                #  "ArnEquals": {
                #    "ec2:Vpc": "arn:aws:ec2:${ var.region }:${ data.aws_caller_identity.current.account_id }:vpc/${ module.red.vpc_id }"
                #    #"ec2:Subnet": "arn:aws:ec2:${ var.region }:${ data.aws_caller_identity.current.account_id }:subnet/${ module.red.private_subnets[0] }"
                #  }
                #}
            }
        ]
    }
    )
}
resource "aws_iam_role_policy_attachment" "lambda_sqs_ec2_role_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.policy-ec2.arn
}

# ############################################
# TRIGGER
# ############################################
resource "aws_lambda_event_source_mapping" "event_source_mapping" {
  event_source_arn = aws_mq_broker.rabbitmq.arn
  enabled          = true
  function_name    = aws_lambda_function.terraform_lambda_func.arn
  batch_size       = 10
  maximum_batching_window_in_seconds = 10
  queues           = [var.queuename]
  source_access_configuration {
    type = "VIRTUAL_HOST"
    uri  = "/"
  }
  source_access_configuration {
    type = "BASIC_AUTH"
    uri  = data.aws_secretsmanager_secret.user_default.arn
  }
  depends_on = [aws_mq_broker.rabbitmq, aws_lambda_function.terraform_lambda_func ]
}
