variable "project_id" {
  description = "The project ID to host the cluster in"
}

variable "bucket_name_suffix" {
  description = "The name suffix for the bucket"
}

