provider "google" {
  project     = var.project_id
  version = "~> 3.18"
}

terraform {
  required_version = "= 0.12.28"
}
