module "gs" {
  source               = "../modules/gs"
  project_id           = var.project_id
  bucket_name_suffix   = var.bucket_name_suffix
}
