variable "project_id" {
  description = "The project ID to host the bucket in"
}

variable "bucket_name_suffix" {
  description = "Bucket name suffix"
}

