resource "google_storage_bucket" "kungfoo-bucket" {
  project       = var.project_id
  name          = "kungfoo-bucket-${var.bucket_name_suffix}"
  force_destroy = true
}