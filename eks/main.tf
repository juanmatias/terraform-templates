
# ############################################
# Providers versions
# ############################################

terraform {
  required_version = "~> 0.12.6"
}

provider "aws" {
  version = "~> 2.43"

  region = "${var.aws_region}"
}

# ############################################
# VPC
# ############################################

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.24.0"

  name = "kungfu-vpc-${var.environment}"
  cidr = "${var.vpc-cidr}"

  azs             = "${var.vpc-azs}"
  public_subnets  = "${var.vpc-public-subnets}"
  private_subnets = "${var.vpc-private-subnets}"
  intra_subnets   = "${var.vpc-intra-subnets}"

  enable_nat_gateway = true
  one_nat_gateway_per_az = true

  enable_dns_hostnames = true

  vpc_tags = {
    "kubernetes.io/cluster/kungfu-${var.environment}-k8s" = "shared"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/kungfu-${var.environment}-k8s" = "shared"
    "kubernetes.io/role/internal-elb" = "1"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/kungfu-${var.environment}-k8s" = "shared"
    "kubernetes.io/role/elb" = "1"
  }

  tags = {
    Terraform = "true"
    Environment = "${var.environment}"
    Project     = "${var.project_name}"
  }
}


# ############################################
# EKS Cluster
# ############################################

module "eks_cluster" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "5.0.0"

  cluster_name    = "kungfu-${var.environment}-k8s"
  subnets         = "${concat(module.vpc.public_subnets, module.vpc.private_subnets)}"
  vpc_id          = "${module.vpc.vpc_id}"
  manage_aws_auth = true
  cluster_version = "${var.k8s-version}"

  worker_groups = [
    {
      name = "worker_group_0"
      instance_type = "${var.k8s-worker-instance0-type}"
      asg_max_size  = 6
      asg_min_size  = 1
      asg_desired_capacity = 2
      enabled_metrics = ["GroupMinSize","GroupMaxSize","GroupDesiredCapacity","GroupInServiceInstances","GroupPendingInstances","GroupTerminatingInstances","GroupStandbyInstances","GroupTotalInstances"]
      enable_monitoring = true
      protect_from_scale_in = false
      autoscaling_enabled = true
      ami_id = "${var.k8s-worker-instance0-amiid}"
    },
    {
      name = "worker_group_1"
      instance_type = "${var.k8s-worker-instance1-type}"
      asg_max_size  = 0
      asg_min_size  = 0
      asg_desired_capacity = 0
      autoscaling_enabled = true
      enabled_metrics = ["GroupMinSize","GroupMaxSize","GroupDesiredCapacity","GroupInServiceInstances","GroupPendingInstances","GroupTerminatingInstances","GroupStandbyInstances","GroupTotalInstances"]
      enable_monitoring = true
      protect_from_scale_in = true
      ami_id = "${var.k8s-worker-instance1-amiid}"
    }
  ]

  map_users =     [
    {
      user_arn = "${var.user-arn}"
      username = "${var.user-name}"
      group    = "system:masters"
    }
  ]

  tags = {
    Terraform = "true"
    Environment = "${var.environment}"
    Project     = "${var.project_name}"
    Component   = "${var.component_label_eks}"
  }
}

# ############################################
# Kube2IAM Roles
# ############################################

# ALB Ingress Controller Role

resource "aws_iam_role" "KungFu-ALBIngressControllerIAMRole" {
  name = "KungFu-ALBIngressControllerIAMRole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal" : {
        "AWS": "${module.eks_cluster.worker_iam_role_arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}


resource "aws_iam_policy" "KungFu-ALBIngressControllerLoadBalancingPolicy" {
  name        = "KungFu-ALBIngressControllerLoadBalancingPolicy"
  path        = "/"
  description = "Grants necessary permissions for running ALB ingress controller on worker nodes."

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "acm:DescribeCertificate",
        "acm:ListCertificates",
        "acm:GetCertificate"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:AuthorizeSecurityGroupIngress",
        "ec2:CreateSecurityGroup",
        "ec2:CreateTags",
        "ec2:DeleteTags",
        "ec2:DeleteSecurityGroup",
        "ec2:DescribeAccountAttributes",
        "ec2:DescribeAddresses",
        "ec2:DescribeInstances",
        "ec2:DescribeInstanceStatus",
        "ec2:DescribeInternetGateways",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeSubnets",
        "ec2:DescribeTags",
        "ec2:DescribeVpcs",
        "ec2:ModifyInstanceAttribute",
        "ec2:ModifyNetworkInterfaceAttribute",
        "ec2:RevokeSecurityGroupIngress"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "elasticloadbalancing:AddListenerCertificates",
        "elasticloadbalancing:AddTags",
        "elasticloadbalancing:CreateListener",
        "elasticloadbalancing:CreateLoadBalancer",
        "elasticloadbalancing:CreateRule",
        "elasticloadbalancing:CreateTargetGroup",
        "elasticloadbalancing:DeleteListener",
        "elasticloadbalancing:DeleteLoadBalancer",
        "elasticloadbalancing:DeleteRule",
        "elasticloadbalancing:DeleteTargetGroup",
        "elasticloadbalancing:DeregisterTargets",
        "elasticloadbalancing:DescribeListenerCertificates",
        "elasticloadbalancing:DescribeListeners",
        "elasticloadbalancing:DescribeLoadBalancers",
        "elasticloadbalancing:DescribeLoadBalancerAttributes",
        "elasticloadbalancing:DescribeRules",
        "elasticloadbalancing:DescribeSSLPolicies",
        "elasticloadbalancing:DescribeTags",
        "elasticloadbalancing:DescribeTargetGroups",
        "elasticloadbalancing:DescribeTargetGroupAttributes",
        "elasticloadbalancing:DescribeTargetHealth",
        "elasticloadbalancing:ModifyListener",
        "elasticloadbalancing:ModifyLoadBalancerAttributes",
        "elasticloadbalancing:ModifyRule",
        "elasticloadbalancing:ModifyTargetGroup",
        "elasticloadbalancing:ModifyTargetGroupAttributes",
        "elasticloadbalancing:RegisterTargets",
        "elasticloadbalancing:RemoveListenerCertificates",
        "elasticloadbalancing:RemoveTags",
        "elasticloadbalancing:SetIpAddressType",
        "elasticloadbalancing:SetSecurityGroups",
        "elasticloadbalancing:SetSubnets",
        "elasticloadbalancing:SetWebACL"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "iam:CreateServiceLinkedRole",
        "iam:GetServerCertificate",
        "iam:ListServerCertificates"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "waf-regional:GetWebACLForResource",
        "waf-regional:GetWebACL",
        "waf-regional:AssociateWebACL",
        "waf-regional:DisassociateWebACL"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "tag:GetResources",
        "tag:TagResources"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "waf:GetWebACL"
      ],
      "Resource": "*"
    }  
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "KungFu-ALBIngressControllerLoadBalancingPolicy_attachement" {
  role       = "${aws_iam_role.KungFu-ALBIngressControllerIAMRole.name}"
  policy_arn = "${aws_iam_policy.KungFu-ALBIngressControllerLoadBalancingPolicy.arn}"
}

# Cluster autoscaler Role

resource "aws_iam_role" "KungFu-ClusterAutoscalerIAMRole" {
  name = "KungFu-ClusterAutoscalerIAMRole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal" : {
        "AWS": "${module.eks_cluster.worker_iam_role_arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "KungFu-ClusterAutoscalerAutoscalingPolicy" {
  name        = "KungFu-ClusterAutoscalerAutoscalingPolicy"
  path        = "/"
  description = "Allow EKS worker nodes to control instances in autoscaling groups."
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeAutoScalingInstances",
        "autoscaling:DescribeLaunchConfigurations",
        "autoscaling:DescribeTags",
        "ec2:DescribeLaunchTemplateVersions"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:SetDesiredCapacity",
        "autoscaling:TerminateInstanceInAutoScalingGroup",
        "autoscaling:UpdateAutoScalingGroup"
      ],
      "Resource": ${jsonencode(module.eks_cluster.workers_asg_arns)}
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "KungFu-ClusterAutoscalerAutoscalingPolicy_attachement" {
  role       = "${aws_iam_role.KungFu-ClusterAutoscalerIAMRole.name}"
  policy_arn = "${aws_iam_policy.KungFu-ClusterAutoscalerAutoscalingPolicy.arn}"
}

# Default Role 

resource "aws_iam_role" "KungFu-DefaultPodIAMRole" {
  name = "KungFu-DefaultPodIAMRole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal" : {
        "AWS": "${module.eks_cluster.worker_iam_role_arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# External DNS Role

resource "aws_iam_role" "KungFu-ExternalDNSIAMRole" {
  name = "KungFu-ExternalDNSIAMRole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal" : {
        "AWS": "${module.eks_cluster.worker_iam_role_arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "KungFu-ExternalDNSPublicHZRoute53Policy" {
  name        = "KungFu-ExternalDNSPublicHZRoute53Policy"
  path        = "/"
  description = "Allow EKS worker nodes to modify Records on R53"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "route53:ListHostedZones",
        "route53:ListResourceRecordSets"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "route53:ChangeResourceRecordSets"
      ],
      "Resource": [
        "arn:aws:route53:::hostedzone/${var.hostedzone-zone_id}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "KungFu-ExternalDNSPublicHZRoute53Policy_attachement" {
  role       = "${aws_iam_role.KungFu-ExternalDNSIAMRole.name}"
  policy_arn = "${aws_iam_policy.KungFu-ExternalDNSPublicHZRoute53Policy.arn}"
}

# ############################################
# Outputs
# ############################################

output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}

output "public_subnet_ids" {
  value = "${module.vpc.public_subnets}"
}

output "private_subnet_ids" {
  value = "${module.vpc.private_subnets}"
}
