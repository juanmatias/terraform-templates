# EKS cluster

This template will deploy an EKS cluster (with vpc, subnets, etc), and a set of Roles ready to be assigned using kube2iam. In this set of roles, there are roles for ALB ingress controllers, external-dns, and cluster autoscaler. 

Feel free to modify the values, but the very basic way to run it is as follows:

```
terraform plan -var user-arn="user arn" -var user-name="user name" -var hostedzone-zone_id="hosted zone id"
```

_Replace the values with the real ones you need._

## Note on why we have two worker nodes groups

There are two groups to allow an almost-no-downtime k8s version migration, as stated [here](https://juanmatiasdelacamara.wordpress.com/?p=620).

## Post deployment tasks


  - Deploy Kube2IAM (the following deployments must be made using kube2iam annotations)
  - Deploy Cluster Autoscaler
  - ALB ingress controller
  - External DNS

### kube2iam

On `assets` directory, there is a `kube2iam.yaml` file.

Look for the _arn_ of the created Default role **KungFu-DefaultPodIAMRole** and replace it in the file:

```
sed -i 's/the-default-role-arn/arn:aws:iam::111111111111:role\/KungFu-DefaultPodIAMRole/' assets/kube2iam.yaml
```

...and apply it:

```
kubectl apply -n kube-system -f assets/kube2iam.yaml
```

### Cluster Autoscaler

On assets you will find the `cluster_autoscaler.yaml` file.

Open it and:

  - modify the aws region
  - modify the `--node-group-auto-discovery` and replace the cluster name, at the end of the line, with the one you have deployed.
  - in case you have modified the cluster autoscaler role, modify the name on `iam.amazonaws.com/role`. In any case you must modify the AWS account.

Then, deploy it:

```
kubectl apply -n kube-system -f cluster_autoscaler.yaml
```

### HELM

Set aconfiguration to run Helm Tiller-less.

Create the namespace:

```
kubectl create ns tiller-ns
```

Export variables and run tiller:

```
export TILLER_NAMESPACE=tiller-ns 
export HELM_HOST=localhost:44134  
/path-to-your-binary/tiller --storage=secret 2>/dev/null 1>/dev/null &
```

_You will need to export the values on each terminal you want to work. If you reboot your pc, or somehow you kill the tiller process, you will need to do again these three steps. You will need to run these steps on each pc you need to work with._

### ALB ingress controller

On assets you will find the file `alb-ingress-controller-values.yaml`, here are the values to deploy the helm chart.

  - Modify the ClusterName if needed.
  - Modify the region if needed.
  - Modify the Role ARN

Deploy the chart:

```
helm install incubator/aws-alb-ingress-controller --name kungfu-aws-alb-ingress-controller --namespace kube-system -f alb-ingress-controller-values.yaml.nosubir
```

### External-DNS

On assets there is a file called `external-dns-values.yaml`.

  - Modify the Role ARN

Deploy it:

```
helm install --name external-dns stable/external-dns -f external-dns-values.yaml
```

## Note on destroying the infra

### Steps

First delete deployments and releases:


```
for f in $(helm list | awk 'FNR > 1 {print $1}' ); do echo "Deleting release: "$f; helm delete --purge $f;  done
for f in $(kubectl get deployment -A | awk 'FNR > 1 {print $1"," $2}'); do deploy=$(echo $f | cut -d',' -f2); ns=$(echo $f | cut -d',' -f1); echo "Deleting $deploy on ns $ns";  kubectl delete deploy $deploy -n $ns; done
```

Deactivate cluster-autoscaler:

```
kubectl scale deployments/cluster-autoscaler --replicas=0 -n kube-system
```

Now do it on two steps:

```
terraform destroy -target=module.eks_cluster 
```

### If you have issues


The second line is suitable for other objects than deployments.... just modify the code.


# Useful links

kube2iam

```
https://github.com/jtblin/kube2iam
```

